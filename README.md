##Changes from base Legion##

####Unit.json files:####
- changed the "buildable_types" and "unit_types" fields to affect new build progression for all factory and fabber units, plus the commander.
- changed all "selection_icon" fields to a positive number. This makes them use the MLA blue circle instead of the Legion red hexagon when hovered over.
- changed T1 and T2 fabbers to use the MLA nanolathe and T3 and T4 ones to use the Legion one ("tools" field).
- changed descriptions to remove all mention of "Legion" and to give "improved" and "elite" descriptions to T2 and T4 factories/fabbers.

####Buildbar icons:####
- replaced control_module (catalyst) icon with a dark red one.
- replaced defense_satellite (anchor) with a dark blue one.

####UI mod changes####
All files in ui/mods/net.cosmicwar.insomnia-legion mirror the files in ui/mods/com.pa.legion-expansion.

- They have all been updated to reflect the new filepath.
- All .js files have been changed so that the red Legion UI can load without checking to see whether we are playing as MLA or Legion (since we don't make this distinction anymore), just whether we have enabled it in the options menu.
- live_game_build_bar.js has also been altered to change the dimensions of the build bar.
- shared_build.js contains the new buildbar layout.
- changes to icon_atlas.js to load all strategic icons at once.
- files in ui/mods/net.cosmicwar.insomnia-legion/img/control_group_bar/red have been replaced with red MLA icons

####Other changes:####
- pa/units/unit_list.json has been updated to remove unwanted units (such as Legion commanders) from the game.
- Various unused folders (such as the ai which no longer works, and the source images for the 'mixed' (purple) UI which we no longer use) have been removed, though there are still some redundant files left in there.




