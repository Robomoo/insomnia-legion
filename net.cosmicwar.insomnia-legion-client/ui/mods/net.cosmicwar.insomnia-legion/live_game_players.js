
var legionExpansionLoaded;

if ( ! legionExpansionLoaded )
{

    legionExpansionLoaded = true;

    function legionExpansion()
    {

        var buildVersion = decode( sessionStorage.build_version );

        var patchName = 'legionExpansion live_game_players.js';

        console.log(patchName + ' on ' + buildVersion + ' last tested on 89755');
        
        var themesetting = api.settings.isSet('ui','legionThemeFunction',true) || 'ON';
        if(themesetting === "ON"){  
			//LOAD CUSTOM LEGION CSS
			loadCSS("coui://ui/mods/net.cosmicwar.insomnia-legion/css/legion_players.css");
			//loadScript("coui://ui/mods/net.cosmicwar.insomnia-legion/common.js");

			//see global.js
			//var legioncomms = legionglobal.commanders;
			//var themesetting = api.settings.isSet('ui','legionThemeFunction',true) || 'ON';

			model.isLegion = ko.computed(function (){
				return true;
			});

			model.isMixed = ko.computed(function (){
				return false;
			});

			model.legionstart = ko.observable(false);

			model.player.subscribe(function(newval){
				if(!model.legionstart()){
					var ui = "legion";
					api.Panel.message("selection","legionui", ui);
					api.Panel.message("planets","legionui", ui);
					api.Panel.message("control_group_bar","legionui", ui);
					api.Panel.message("econ","legionui", ui);
					api.Panel.message("options_bar","legionui", ui);
					api.Panel.message("build_hover","legionui", ui);
					api.Panel.message("time_bar","legionui", ui);
					api.Panel.message("menu","legionui", ui);
					
					var toggleImage = function(open) {
						return open ? 'coui://ui/mods/net.cosmicwar.insomnia-legion/img/controls/red/pin_open.png' : 'coui://ui/mods/net.cosmicwar.insomnia-legion/img/controls/red/pin_closed.png';
					};
					
					model.playerPanelToggleImage = ko.computed(function() { return toggleImage(model.showPlayerListPanel()); });
					model.spectatorPanelToggleImage = ko.computed(function() { return toggleImage(model.showSpectatorPanel()); });

					
					$('img[src="coui://ui/main/shared/img/controls/pin_open.png"]').attr("src","coui://ui/mods/net.cosmicwar.insomnia-legion/img/controls/red/pin_open.png");
					$('img[src="coui://ui/main/shared/img/controls/pin_closed.png"]').attr("src","coui://ui/mods/net.cosmicwar.insomnia-legion/img/controls/red/pin_closed.png");
					
					model.legionstart(true);
				}
			});
			
			$('.body_panel').attr("data-bind","css: { legionui: model.isLegion(), mixedui: model.isMixed()}, visible: show");
			
			//COMMANDER IMAGE
			model.commanderImage = function (d){
				return "coui://ui/main/game/live_game/img/players_list_panel/icon_player_outline.png";
			}
			
			model.commanderImageMaskLeg = function (d){
				 return true;
			}
			
			model.commanderImageMaskMix = function (d){
				 return false;
			}
			
			$('img[src="img/players_list_panel/icon_player_outline.png"]').replaceWith('<img data-bind="attr:{src: model.commanderImage($data)}" />');
			$('.player_masked').attr("data-bind","style: { backgroundColor: color }, css: { legcom: model.commanderImageMaskLeg($data), mixcom: model.commanderImageMaskMix($data)}");
		}
        
    }

    try
    {
        legionExpansion();
    }
    catch (e)
    {
        console.log(e);
        console.log(JSON.stringify(e));
    }
}