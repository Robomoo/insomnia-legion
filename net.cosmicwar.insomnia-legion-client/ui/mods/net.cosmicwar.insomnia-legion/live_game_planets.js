
var legionExpansionLoaded;

if ( ! legionExpansionLoaded )
{

    legionExpansionLoaded = true;

    function legionExpansion()
    {

        var buildVersion = decode( sessionStorage.build_version );

        var patchName = 'legionExpansion live_game_planets.js';

        console.log(patchName + ' on ' + buildVersion + ' last tested on 89755');

        var themesetting = api.settings.isSet('ui','legionThemeFunction',true) || 'ON';
        if(themesetting === "ON"){  
            loadCSS("coui://ui/mods/net.cosmicwar.insomnia-legion/css/legion_planets.css");
			
			$('.body_panel').addClass("legionui");
                
			model.toggleImage = ko.computed(function() {
				return model.showCelestialViewModels() ? 'coui://ui/mods/net.cosmicwar.insomnia-legion/img/controls/red/pin_open.png' : 'coui://ui/mods/net.cosmicwar.insomnia-legion/img/controls/red/pin_closed.png';
			});  
			$('img[src="coui://ui/main/shared/img/controls/pin_open.png"]').attr("src","coui://ui/mods/net.cosmicwar.insomnia-legion/img/controls/red/pin_open.png");
			$('img[src="coui://ui/main/shared/img/controls/pin_closed.png"]').attr("src","coui://ui/mods/net.cosmicwar.insomnia-legion/img/controls/red/pin_closed.png");
        }
    }

    try
    {
        legionExpansion();
    }
    catch (e)
    {
        console.log(e);
        console.log(JSON.stringify(e));
    }
}