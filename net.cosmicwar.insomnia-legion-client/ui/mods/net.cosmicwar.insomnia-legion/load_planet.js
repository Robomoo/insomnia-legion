var themesetting = api.settings.isSet('ui','legionMenuThemeFunction',true) || 'ON';
if(themesetting === "ON"){
    //load legion theme
    loadCSS("coui://ui/mods/net.cosmicwar.insomnia-legion/css/legion_buttons.css");
    loadCSS("coui://ui/mods/net.cosmicwar.insomnia-legion/css/legion_shared.css");
    loadCSS("coui://ui/mods/net.cosmicwar.insomnia-legion/css/background_no_logo.css");
    loadCSS("coui://ui/mods/net.cosmicwar.insomnia-legion/css/load_planet.css");    
    $('body').addClass("legion");
}