var legionExpansionLoaded;

if ( ! legionExpansionLoaded )
{

    legionExpansionLoaded = true;

    function legionExpansion()
    {
		model.BuildTab.ROWS_PER_TAB = 6;
		
		if (model.BuildSet && model.BuildSet.tabsTemplate) {
			model.BuildSet.tabsTemplate = [
				['factory', '!LOC:factory', true],
				['combat', '!LOC:combat', false],
				['utility', '!LOC:utility', false],
				['vehicle', '!LOC:vehicle'],
				['bot', '!LOC:bot'],
				['air', '!LOC:air'],
				['sea', '!LOC:sea'],
				['orbital', '!LOC:orbital', true],
				['orbital_structure', 'orbital structure', true],
				['ammo', '!LOC:ammo', true]
			];
		}
		
        var themesetting = api.settings.isSet('ui','legionThemeFunction',true) || 'ON';
        if(themesetting === "ON"){ 
            //LOAD CUSTOM LEGION BUILDBAR CSS
            loadCSS("coui://ui/mods/net.cosmicwar.insomnia-legion/css/legion_build_bar.css");

            model.isLegion = function (data){
                return true;
            };

            model.isMixed = function (data){
                return false;
            };

            //ADD legion / mixed ui
            $('.body_panel').attr("data-bind","css: { legion: model.isLegion($data), mixed: model.isMixed($data)}");
        }

    }

    try
    {
        legionExpansion();
    }
    catch (e)
    {
        console.log(e);
        console.log(JSON.stringify(e));
    }
}
