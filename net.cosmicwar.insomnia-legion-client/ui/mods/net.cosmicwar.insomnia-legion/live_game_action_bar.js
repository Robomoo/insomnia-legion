var legionExpansionLoaded;

if ( ! legionExpansionLoaded )
{

    legionExpansionLoaded = true;

    function legionExpansion()
    {

        var buildVersion = decode( sessionStorage.build_version );

        var patchName = 'legionExpansion live_game_action_bar.js';

        console.log(patchName + ' on ' + buildVersion + ' last tested on 89755');

        var themesetting = api.settings.isSet('ui','legionThemeFunction',true) || 'ON';
        if(themesetting === "ON"){  
            //LOAD CUSTOM LEGION ACTIONBAR CSS
            loadCSS("coui://ui/mods/net.cosmicwar.insomnia-legion/css/legion_action_bar.css");

            model.isLegion = function (data){
                return true;
            };

            model.isMixed = function (data){
                return false;
            };

            //ADD legion class to build_bar_menu
            $('.body_panel').attr("data-bind","css: { legion: model.isLegion($data), mixed: model.isMixed($data)}");
        }
    }

    try
    {
        legionExpansion();
    }
    catch (e)
    {
        console.log(e);
        console.log(JSON.stringify(e));
    }
}
