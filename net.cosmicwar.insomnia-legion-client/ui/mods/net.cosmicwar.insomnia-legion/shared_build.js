var legionExpansion_sharedBuildLoaded;
if(!legionExpansion_sharedBuildLoaded){
	
	legionExpansion_sharedBuildLoaded = true;
	
	function legionExpansion() {
		Build.HotkeyModel.SpecIdToGridMap = {
			//UTILITY////////////////////////////////////////////////
			"/pa/units/land/energy_plant_adv/energy_plant_adv.json": ["utility", 15],
			"/pa/units/land/metal_extractor_adv/metal_extractor_adv.json": ["utility", 16],			
			"/pa/units/land/radar_adv/radar_adv.json": ["utility", 20],
			"/pa/units/land/radar/radar.json": ["utility", 32],
			"/pa/units/land/energy_plant/energy_plant.json": ["utility", 27],
			"/pa/units/land/metal_extractor/metal_extractor.json": ["utility", 28],
			"/pa/units/land/land_barrier/land_barrier.json": ["utility", 30],
			"/pa/units/land/teleporter/teleporter.json": ["utility", 31],
			"/pa/units/land/energy_storage/energy_storage.json": ["utility", 33],
			"/pa/units/land/metal_storage/metal_storage.json": ["utility", 34],
			
			"/pa/units/land/l_energy_plant_adv/l_energy_plant_adv.json": [ "utility", 9 ],
			"/pa/units/land/l_mex_adv/l_mex_adv.json": [ "utility", 10 ],
			
			"/pa/units/land/l_radar_adv/l_radar_adv.json": [ "utility", 14 ],
			"/pa/units/land/l_energy_plant/l_energy_plant.json": [ "utility", 21 ],
			"/pa/units/land/l_mex/l_mex.json": [ "utility", 22 ],
			"/pa/units/land/l_land_barrier/l_land_barrier.json": [ "utility", 24 ],
			"/pa/units/land/l_teleporter/l_teleporter.json": [ "utility", 25 ],
			"/pa/units/land/l_radar/l_radar.json": [ "utility", 26 ],
			"/pa/units/land/l_storage/l_storage.json": [ "utility", 35 ],
			"/pa/units/land/l_shield_gen/l_shield_gen.json": [ "utility", 18 ],
			
			"/pa/units/orbital/l_mining_platform/l_mining_platform.json": [ "utility", 23 ],
			"/pa/units/orbital/mining_platform/mining_platform.json": ["utility", 29],
			"/pa/units/air/l_flying_teleporter/l_flying_teleporter.json": [ "utility", 19 ],
			
			"/pa/units/land/l_titan_structure/l_titan_structure.json": [ "utility", 6 ],
			"/pa/units/land/titan_structure/titan_structure.json": ["utility", 12],
			
			
			//"/pa/units/orbital/l_deep_space_radar/l_deep_space_radar.json": [ "utility", 14 ],
			

			//FACTORY//////////////////////////////////////////////////
			"/pa/units/land/l_titan_bot/l_titan_bot.json": [ "factory", 0 ],
			"/pa/units/land/l_titan_vehicle/l_titan_vehicle.json": [ "factory", 1 ],
			//naval
			"/pa/units/air/l_titan_air/l_titan_air.json": [ "factory", 3 ],
			"/pa/units/orbital/l_titan_orbital/l_titan_orbital.json": [ "factory", 4 ],
			
			
			"/pa/units/land/titan_bot/titan_bot.json": ["factory", 6],
			"/pa/units/land/titan_vehicle/titan_vehicle.json": ["factory", 7],
			//naval
			"/pa/units/air/titan_air/titan_air.json": ["factory", 9],
			"/pa/units/orbital/titan_orbital/titan_orbital.json": ["factory", 10],
			
			"/pa/units/land/l_bot_factory_adv/l_bot_factory_adv.json": [ "factory", 12 ],
			"/pa/units/land/l_vehicle_factory_adv/l_vehicle_factory_adv.json": [ "factory", 13 ],
			"/pa/units/sea/l_naval_factory_adv/l_naval_factory_adv.json": [ "factory", 14 ],
			"/pa/units/air/l_air_factory_adv/l_air_factory_adv.json": [ "factory", 15 ],
			"/pa/units/orbital/l_orbital_factory/l_orbital_factory.json": [ "factory", 16 ],
			
			"/pa/units/land/bot_factory_adv/bot_factory_adv.json": ["factory", 18],
			"/pa/units/land/vehicle_factory_adv/vehicle_factory_adv.json": ["factory", 19],
			"/pa/units/sea/naval_factory_adv/naval_factory_adv.json": ["factory", 20],
			"/pa/units/air/air_factory_adv/air_factory_adv.json": ["factory", 21],
			"/pa/units/orbital/orbital_factory/orbital_factory.json": ["factory", 22],
			
			"/pa/units/land/l_bot_factory/l_bot_factory.json": [ "factory", 24 ],
			"/pa/units/land/l_vehicle_factory/l_vehicle_factory.json": [ "factory", 25 ],
			"/pa/units/sea/l_naval_factory/l_naval_factory.json": [ "factory", 26 ],
			"/pa/units/air/l_air_factory/l_air_factory.json": [ "factory", 27 ],
			"/pa/units/orbital/l_orbital_launcher/l_orbital_launcher.json": [ "factory", 28 ],
			
			"/pa/units/land/bot_factory/bot_factory.json": ["factory", 30],
			"/pa/units/land/vehicle_factory/vehicle_factory.json": ["factory", 31],			
			"/pa/units/sea/naval_factory/naval_factory.json": ["factory", 32],
			"/pa/units/air/air_factory/air_factory.json": ["factory", 33],
			"/pa/units/orbital/orbital_launcher/orbital_launcher.json": ["factory", 34],
			
			
			//COMBAT////////////////////////////////////////////////////
			"/pa/units/land/control_module/control_module.json": ["combat", 0],
			"/pa/units/land/l_anti_nuke_launcher/l_anti_nuke_launcher.json": [ "combat", 1 ],
			"/pa/units/land/l_nuke_launcher/l_nuke_launcher.json": [ "combat", 2 ],
			"/pa/units/land/l_rocket_barrage/l_rocket_barrage.json": [ "combat", 3 ],
			"/pa/units/orbital/l_delta_v_engine/l_delta_v_engine.json": [ "combat", 5 ],
			
			"/pa/units/land/laser_defense_adv/laser_defense_adv.json": ["combat", 6],
			"/pa/units/land/anti_nuke_launcher/anti_nuke_launcher.json": ["combat", 7],
			"/pa/units/land/nuke_launcher/nuke_launcher.json": ["combat", 8],
			"/pa/units/land/tactical_missile_launcher/tactical_missile_launcher.json": ["combat", 9],
			"/pa/units/land/l_flame_turret/l_flame_turret.json": [ "combat", 10 ],
			"/pa/units/orbital/delta_v_engine/delta_v_engine.json": ["combat", 11],
			
			"/pa/units/land/l_t1_turret_adv/l_t1_turret_adv.json": [ "combat", 12 ],
			"/pa/units/land/l_air_defense_adv/l_air_defense_adv.json": [ "combat", 13 ],			
			"/pa/units/sea/l_torpedo_launcher_adv/l_torpedo_launcher_adv.json": [ "combat", 14 ],
			"/pa/units/orbital/l_defense_satellite/l_defense_satellite.json": [ "combat", 15 ],
			"/pa/units/land/l_artillery_long/l_artillery_long.json": [ "combat", 16 ],
			"/pa/units/orbital/l_orbital_dropper/l_orbital_dropper.json": [ "combat", 17 ],
			
			"/pa/units/land/l_t1_turret_basic/l_t1_turret_basic.json": [ "combat", 18 ],
			"/pa/units/land/air_defense_adv/air_defense_adv.json": ["combat", 19],
			"/pa/units/sea/torpedo_launcher_adv/torpedo_launcher_adv.json": ["combat", 20],
			"/pa/units/orbital/defense_satellite/defense_satellite.json": ["combat", 21],
			"/pa/units/land/artillery_long/artillery_long.json": ["combat", 22],
			"/pa/units/land/unit_cannon/unit_cannon.json": ["combat", 23],
			
			"/pa/units/land/laser_defense/laser_defense.json": ["combat", 24],
			"/pa/units/land/l_air_defense/l_air_defense.json": [ "combat", 25 ],
			"/pa/units/sea/l_torpedo_launcher/l_torpedo_launcher.json": [ "combat", 26 ],
			"/pa/units/orbital/l_ion_defense/l_ion_defense.json": [ "combat", 27 ],
			"/pa/units/land/l_artillery_short/l_artillery_short.json": [ "combat", 28 ],
			"/pa/units/land/l_swarm_hive/l_swarm_hive.json": [ "combat", 29 ],
			
			"/pa/units/land/laser_defense_single/laser_defense_single.json": ["combat", 30],
			"/pa/units/land/air_defense/air_defense.json": ["combat", 31],
			"/pa/units/sea/torpedo_launcher/torpedo_launcher.json": ["combat", 32],
			"/pa/units/orbital/ion_defense/ion_defense.json": ["combat", 33],
			"/pa/units/land/artillery_short/artillery_short.json": ["combat", 34],
			"/pa/units/land/artillery_unit_launcher/artillery_unit_launcher.json": ["combat", 35],
			
			
			//AMMO/////////////////////////////////////////////////////////////
			"/pa/units/land/l_land_mine/l_land_mine.json": [ "ammo", 24 ],
			"/pa/units/sea/l_sea_mine/l_sea_mine.json": [ "ammo", 25 ],
			"/pa/units/land/l_anti_nuke_launcher/l_anti_nuke_launcher_ammo.json": [ "ammo", 27 ],
			"/pa/units/land/l_nuke_launcher/l_nuke_launcher_ammo.json": [ "ammo", 28 ],
			
			"/pa/units/land/land_mine/land_mine.json": ["ammo", 30],
			"/pa/units/land/anti_nuke_launcher/anti_nuke_launcher_ammo.json": ["ammo", 33],
			"/pa/units/land/nuke_launcher/nuke_launcher_ammo.json": ["ammo", 34],
			
		
			//VEHICLE//////////////////////////////////////////////////////////
			"/pa/units/land/tank_nuke/tank_nuke.json": ["vehicle", 6],
			"/pa/units/land/l_tank_swarm/l_tank_swarm.json": [ "vehicle", 7 ],
			
			"/pa/units/land/l_fabrication_vehicle_adv/l_fabrication_vehicle_adv.json": [ "vehicle", 12 ],
			"/pa/units/land/l_tank_laser_adv/l_tank_laser_adv.json": [ "vehicle", 13 ],
			"/pa/units/land/l_tank_heavy_armor/l_tank_heavy_armor.json": [ "vehicle", 14 ],
			"/pa/units/land/l_sniper_tank/l_sniper_tank.json": [ "vehicle", 15 ],
			"/pa/units/land/l_hover_tank_adv/l_hover_tank_adv.json": [ "vehicle", 16 ],
			
			"/pa/units/land/fabrication_vehicle_adv/fabrication_vehicle_adv.json": ["vehicle", 18],
			"/pa/units/land/tank_laser_adv/tank_laser_adv.json": ["vehicle", 19],
			"/pa/units/land/tank_heavy_armor/tank_heavy_armor.json": ["vehicle", 20],
			"/pa/units/land/tank_heavy_mortar/tank_heavy_mortar.json": ["vehicle", 21],
			"/pa/units/land/tank_flak/tank_flak.json": ["vehicle", 22],
			
			"/pa/units/land/l_fabrication_vehicle/l_fabrication_vehicle.json": [ "vehicle", 24 ],
			"/pa/units/land/l_tank_shank/l_tank_shank.json": [ "vehicle", 25 ],
			"/pa/units/land/l_shotgun_tank/l_shotgun_tank.json": [ "vehicle", 26 ],
			"/pa/units/land/l_mortar_tank/l_mortar_tank.json": [ "vehicle", 27 ],
			"/pa/units/land/l_hover_tank/l_hover_tank.json": [ "vehicle", 28 ],
			"/pa/units/land/l_fabrication_vehicle_combat/l_fabrication_vehicle_combat.json": [ "vehicle", 29 ],
			
			"/pa/units/land/fabrication_vehicle/fabrication_vehicle.json": ["vehicle", 30],
			"/pa/units/land/tank_light_laser/tank_light_laser.json": ["vehicle", 31],
			"/pa/units/land/tank_armor/tank_armor.json": ["vehicle", 32],
			"/pa/units/land/land_scout/land_scout.json": ["vehicle", 33],
			"/pa/units/land/aa_missile_vehicle/aa_missile_vehicle.json": ["vehicle", 34],
			"/pa/units/land/tank_hover/tank_hover.json": ["vehicle", 35],

			
			//BOT/////////////////////////////////////////////////////////////
			"/pa/units/land/bot_support_commander/bot_support_commander.json": ["bot", 6],
			"/pa/units/land/l_bot_support_commander/l_bot_support_commander.json": [ "bot", 7 ],
			
			"/pa/units/land/l_fabrication_bot_adv/l_fabrication_bot_adv.json": [ "bot", 12 ],
			"/pa/units/land/l_riot_bot/l_riot_bot.json": [ "bot", 13 ],
			"/pa/units/land/l_bot_artillery_adv/l_bot_artillery_adv.json": [ "bot", 14 ],
			"/pa/units/land/l_bot_aa_adv/l_bot_aa_adv.json": [ "bot", 15 ],
			"/pa/units/land/l_bot_artillery/l_bot_artillery.json": [ "bot", 16 ],
			"/pa/units/land/l_necromancer/l_necromancer.json": [ "bot", 17 ],
			
			"/pa/units/land/fabrication_bot_adv/fabrication_bot_adv.json": ["bot", 18],
			"/pa/units/land/assault_bot_adv/assault_bot_adv.json": ["bot", 19],
			"/pa/units/land/bot_sniper/bot_sniper.json": ["bot", 20],
			"/pa/units/land/fabrication_bot_combat_adv/fabrication_bot_combat_adv.json": ["bot", 21],
			"/pa/units/land/bot_tactical_missile/bot_tactical_missile.json": ["bot", 22],
			"/pa/units/land/bot_nanoswarm/bot_nanoswarm.json": ["bot", 23],
			
			"/pa/units/land/l_fabrication_bot/l_fabrication_bot.json": [ "bot", 24 ],
			"/pa/units/land/l_assault_bot/l_assault_bot.json": [ "bot", 25 ],
			"/pa/units/land/l_sniper_bot/l_sniper_bot.json": [ "bot", 26 ],
			"/pa/units/land/l_bot_aa/l_bot_aa.json": [ "bot", 27 ],
			"/pa/units/land/l_bot_bomb/l_bot_bomb.json": [ "bot", 28 ],
			"/pa/units/land/l_scout_bot/l_scout_bot.json": [ "bot", 29 ],

			"/pa/units/land/fabrication_bot/fabrication_bot.json": ["bot", 30],
			"/pa/units/land/assault_bot/assault_bot.json": ["bot", 31],
			"/pa/units/land/bot_grenadier/bot_grenadier.json": ["bot", 32],
			"/pa/units/land/fabrication_bot_combat/fabrication_bot_combat.json": ["bot", 33],
			"/pa/units/land/bot_bomb/bot_bomb.json": ["bot", 34],
			"/pa/units/land/bot_tesla/bot_tesla.json": ["bot", 35],


			//AIR//////////////////////////////////////////////////////////////////
			"/pa/units/air/support_platform/support_platform.json": ["air", 6],
			"/pa/units/air/l_air_carrier/l_air_carrier.json": [ "air", 7 ],

			"/pa/units/air/l_fabrication_aircraft_adv/l_fabrication_aircraft_adv.json": [ "air", 12 ],
			"/pa/units/air/l_fighter_adv/l_fighter_adv.json": [ "air", 13 ],
			"/pa/units/air/l_gunship/l_gunship.json": [ "air", 14 ],
			"/pa/units/air/l_air_scout_adv/l_air_scout_adv.json": [ "air", 15 ],
			"/pa/units/air/l_firestarter/l_firestarter.json": [ "air", 16 ],

			"/pa/units/air/fabrication_aircraft_adv/fabrication_aircraft_adv.json": ["air", 18],
			"/pa/units/air/fighter_adv/fighter_adv.json": ["air", 19],
			"/pa/units/air/gunship/gunship.json": ["air", 20],
			"/pa/units/air/bomber_adv/bomber_adv.json": ["air", 21],
			"/pa/units/air/bomber_heavy/bomber_heavy.json": ["air", 22],
			
			"/pa/units/air/l_fabrication_aircraft/l_fabrication_aircraft.json": [ "air", 24 ],
			"/pa/units/air/l_fighter/l_fighter.json": [ "air", 25 ],
			"/pa/units/air/l_bomber/l_bomber.json": [ "air", 26 ],
			"/pa/units/air/l_raider/l_raider.json": [ "air", 27 ],
			"/pa/units/air/l_transport/l_transport.json": [ "air", 28 ],
			"/pa/units/air/l_air_bomb/l_air_bomb.json": [ "air", 29 ],

			"/pa/units/air/fabrication_aircraft/fabrication_aircraft.json": ["air", 30],
			"/pa/units/air/fighter/fighter.json": ["air", 31],
			"/pa/units/air/bomber/bomber.json": ["air", 32],
			"/pa/units/air/air_scout/air_scout.json": ["air", 33],
			"/pa/units/air/transport/transport.json": ["air", 34],
			"/pa/units/air/solar_drone/solar_drone.json": ["air", 35],
			
			//"/pa/units/air/l_air_scout_adv/vision/vision.json": [ "air", 0 ],
			
			
			//SEA//////////////////////////////////////////////////////////////////
			"/pa/units/sea/drone_carrier/carrier/carrier.json": ["sea",6],
			"/pa/units/sea/l_hover_ship/l_hover_ship.json": [ "sea", 7 ],
			
			"/pa/units/sea/l_fabrication_ship_adv/l_fabrication_ship_adv.json": [ "sea", 12 ],
			"/pa/units/sea/l_sea_tank/l_sea_tank.json": [ "sea", 13 ],
			"/pa/units/sea/l_battleship/l_battleship.json": [ "sea", 14 ],
			"/pa/units/sea/l_missile_ship/l_missile_ship.json": [ "sea", 15 ],
			"/pa/units/sea/l_fabrication_sub_combat_adv/l_fabrication_sub_combat_adv.json": [ "sea", 16 ],

			"/pa/units/sea/fabrication_ship_adv/fabrication_ship_adv.json": ["sea",18],
			"/pa/units/sea/battleship/battleship.json": ["sea", 19],
			"/pa/units/sea/missile_ship/missile_ship.json": ["sea", 20],
			"/pa/units/sea/nuclear_sub/nuclear_sub.json": ["sea", 21],
			"/pa/units/sea/hover_ship/hover_ship.json": ["sea",22],
			
			"/pa/units/sea/l_fabrication_ship/l_fabrication_ship.json": [ "sea", 24 ],
			"/pa/units/sea/l_sea_scout/l_sea_scout.json": [ "sea", 25 ],
			"/pa/units/sea/l_destroyer/l_destroyer.json": [ "sea", 26 ],
			"/pa/units/sea/l_attack_sub/l_attack_sub.json": [ "sea", 27 ],
			"/pa/units/sea/l_frigate/l_frigate.json": [ "sea", 28 ],

			"/pa/units/sea/fabrication_ship/fabrication_ship.json": ["sea", 30],
			"/pa/units/sea/frigate/frigate.json": ["sea", 31],
			"/pa/units/sea/destroyer/destroyer.json": ["sea", 32],
			"/pa/units/sea/attack_sub/attack_sub.json": ["sea", 33],
			"/pa/units/sea/sea_scout/sea_scout.json": ["sea", 34],
			"/pa/units/sea/fabrication_barge/fabrication_barge.json": ["sea",35],


			//ORBITAL///////////////////////////////////////////////////////////
			"/pa/units/orbital/orbital_battleship/orbital_battleship.json": ["orbital", 6],
			"/pa/units/orbital/l_orbital_battleship/l_orbital_battleship.json": [ "orbital", 7 ],
			
			"/pa/units/orbital/l_orbital_laser/l_orbital_laser.json": [ "orbital", 12 ],
			"/pa/units/orbital/l_radar_satellite_adv/l_radar_satellite_adv.json": [ "orbital", 13 ],
			"/pa/units/orbital/l_orbital_railgun/l_orbital_railgun.json": [ "orbital", 14 ],

			"/pa/units/orbital/orbital_laser/orbital_laser.json": ["orbital", 18],
			"/pa/units/orbital/radar_satellite_adv/radar_satellite_adv.json": ["orbital", 19],
			"/pa/units/orbital/orbital_railgun/orbital_railgun.json": ["orbital", 20],
			"/pa/units/orbital/solar_array/solar_array.json": ["orbital", 21],
			
			"/pa/units/orbital/l_orbital_fabrication_bot/l_orbital_fabrication_bot.json": [ "orbital", 24 ],
			"/pa/units/orbital/l_orbital_fighter/l_orbital_fighter.json": [ "orbital", 25 ],
			"/pa/units/orbital/l_radar_satellite/l_radar_satellite.json": [ "orbital", 26 ],
			"/pa/units/orbital/l_orbital_lander/l_orbital_lander.json": [ "orbital", 27 ],
			"/pa/units/orbital/l_orbital_probe/l_orbital_probe.json": [ "orbital", 28 ],			

			"/pa/units/orbital/orbital_fabrication_bot/orbital_fabrication_bot.json": ["orbital", 30],
			"/pa/units/orbital/orbital_fighter/orbital_fighter.json": ["orbital", 31],
			"/pa/units/orbital/radar_satellite/radar_satellite.json": ["orbital", 32],
			"/pa/units/orbital/orbital_lander/orbital_lander.json": ["orbital", 33],
			"/pa/units/orbital/orbital_probe/orbital_probe.json": ["orbital", 34],
		};
	}
	
	try
    {
        legionExpansion();
    }
    catch (e)
    {
        console.log(e);
        console.log(JSON.stringify(e));
    }
}